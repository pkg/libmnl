libmnl (1.0.5-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 14:35:16 +0000

libmnl (1.0.5-3) unstable; urgency=medium

  * [63fb516] d/.gitignore: add Emacs artefacts
  * [dc87fce] d/control: update my e-mail address
  * [e6475e3] d/control: bump Standards-Version to 4.7.0
  * [d08a2cf] d/rules: set locale to fix man-page reproducibility
  * [f4671d2] d/control: replace pkg-config dependencies with pkgconf
  * [5ca6d57] d/gbp.conf: add `[DEFAULT]` section for pristine-tar,
    debian-branch & upstream-branch

 -- Jeremy Sowden <azazel@debian.org>  Wed, 18 Sep 2024 20:18:11 +0100

libmnl (1.0.5-2) unstable; urgency=medium

  * Re-upload with source only, to unblock the migration policy

 -- Neutron Soutmun <neutrons@debian.org>  Mon, 06 Nov 2023 15:21:59 +0700

libmnl (1.0.5-1) unstable; urgency=medium

  [ Jeremy Sowden ]
  * [669ecab] New upstream version 1.0.5
    (closes: #1053156)
    + Features:
      - New examples
      - MNL_SOCKET_DUMP_SIZE define, holding a recommended buffer size for
        netlink dumps
    + Fixes:
      - nfct-daemon example compile error with musl libc
      - Compiler warning when passing a const 'cb_ctl_array' to mnl_cb_run2()
      - Typo in rtnl-addr-dump example
      - Valgrind warnings due to uninitialized padding in netlink messages
      - Misc fixes in doxygen documentation
      - Misc build system fixes
  * [12ac3a4] d/watch: update to v4
  * [da939fe] d/watch: update URL
  * [6ed4fbb] d/upstream/signing-key.asc: update to current minimal key
  * [719eaab] d/control: bump Standards-Version to 4.6.0
  * [d511ad0] d/control: bump debhelper-compat to 13
  * [8f0a9b2] d/control: add myself to uploaders
  * [6622ceb] d/control: add `Rules-Requires-Root: no`
  * [00f30c6] d/control: correct `Multi-Arch:` capitalization
  * [195c07d] d/*.install: replace globs with multi-arch variables
  * [dd3d20a] d/not-installed: ignore libtool archive
  * [e2a70f2] d/rules, d/libmnl-dev.install: build and install static library
    (closes: #1010616)
  * [09b7021] d/rules: remove explicit export of dpkg build flags
  * [67f8714] d/libmnl0.symbols: add `Build-Depends-Package:` field
  * [284d1a5] d/libmnl-dev.exmaples: install examples
  * [e9ba264] d/gbp.conf: buildpackage, import-orig: enable `pristine-tar`
  * [3921355] d/gbp.conf: dch: set `commit-msg` and `id-length`
  * [483adb5] d/copyright: update copyright field for debian/*
  * [9759d67] d/control: bump Standards-Version to 4.6.1
  * [4f81a46] Add libmnl-doc package containing man-pages and HTML
    documentation
  * [7913abf] d/patches: add a patch to fix documentation typo's
  * [af40972] d/.gitignore: add file to ignore build artefacts
  * [65cac12] d/control: bump Standards-Version to 4.6.2
  * [d423c8e] d/control: move -doc build-dep's to `Build-Depends-Indep:`
  * [2faddb7] d/rules: add support for `<nodoc>` build-profile

  [ Neutron Soutmun ]
  * [bf14b23] d/gbp.conf: add gbp.conf options

 -- Neutron Soutmun <neutrons@debian.org>  Sat, 04 Nov 2023 14:35:17 +0700

libmnl (1.0.4-3+apertis1) apertis; urgency=medium

  * Refresh the automatically detected licensing information

 -- Walter Lozano <walter.lozano@collabora.com>  Thu, 19 Aug 2021 06:14:05 -0300

libmnl (1.0.4-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 08:56:18 +0000

libmnl (1.0.4-3) unstable; urgency=medium

  [ Arturo Borrero Gonzalez ]
  * [e542e5f] src:libmnl: reallocate to pkg-netfilter packaging team (and salsa)
    (Closes: #925099)

  [ Alberto Molina Coballes ]
  * [fc7598a] d/control: mark libmnl-dev as Multi-Arch: same (Closes: #942329)

  [ Debian Janitor ]
  * [b91f74f] Trim trailing whitespace.
  * [392bb25] Use secure copyright file specification URI.
  * [01df844] Drop custom source compression.
  * [1183b8e] Use secure URI in Homepage field.
  * [b2a05fb] Bump debhelper from old 9 to 10.
  * [2a532d8] Drop unnecessary dependency on dh-autoreconf.
  * [1e14bfb] Use canonical URL in Vcs-Git.
  * [5f69001] Fix day-of-week for changelog entry 1.0.3-2.

  [ Arturo Borrero Gonzalez ]
  * [c79e0db] d/: add salsa gitlab integration
  * [3526970] src:libmnl: drop dh-exec build-dep
  * [80e16a6] src:libmnl: bump std-version to 4.5.0
  * [7e80c86] src:libmnl: bump debhelper-compat to 12 and refresh build-deps
  * [825c810] src:libmnl: downgrade the priority of the package

 -- Arturo Borrero Gonzalez <arturo@debian.org>  Tue, 14 Apr 2020 12:09:01 +0200

libmnl (1.0.4-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:16:01 +0000

libmnl (1.0.4-2) unstable; urgency=medium

  * Update debian/copyright
    * The upstream has changed the license of libmnl from GPL-2+ to LGPL-2.1
      Thanks to Jean-Baptiste Rouault who pointing this [1] (Closes: #839721)
      [1] https://git.netfilter.org/libmnl/commit/?id=a2975d2
    * Update my email and copyright year.

 -- Neutron Soutmun <neutrons@debian.org>  Wed, 05 Oct 2016 23:47:51 +0700

libmnl (1.0.4-1) unstable; urgency=medium

  * Imported Upstream version 1.0.4
    - Fix FTBFS with clang [1] (Closes: #789480)
      [1] https://git.netfilter.org/libmnl/commit/?id=dcdb473
  * Drop debian/patches/update-libtool.patch, updated upstream
  * Change my email to neutrons@debian.org
  * Update Vcs-* with secured URI
  * Bump Standards-Version to 3.9.8, no changes needed
  * Update debian/libmnl0.symbols
    * Add new version and new API function symbols
  * Bump package priority to "important" (Closes: #799255)
    * Prepare for the next debian revision of "iptables" package as
      requested by Arturo Borrero Gonzalez <arturo.borrero.glez@gmail.com>

 -- Neutron Soutmun <neutrons@debian.org>  Mon, 08 Aug 2016 14:44:16 +0700

libmnl (1.0.3-5) unstable; urgency=medium

  * Drop the NEWS as it's considered doesn't make any senses
    * debian/NEWS:
      - Drop as it's considered doesn't make any senses and no interest for
        the end-user anyway. Thanks to David Prévot who reported.
        (Closes: #756967)

 -- Neutron Soutmun <neo.neutron@gmail.com>  Mon, 04 Aug 2014 09:59:54 +0700

libmnl (1.0.3-4) unstable; urgency=low

  * debian/control: Drop Dm-Upload-Allowed as it is obsoleted
  * Make package more backports friendly, bump Standards-Version
    * debian/control:
      - Build-deps on debhelper (>= 9~), more backports friendly.
      - Bump Standards-Version to 3.9.5, no changes needed.
  * Apply wrap-and-sort -s
  * Drop unnecessary include for buildflags.mk
    * debian/rules:
      - Drop DPKG_EXPORT_BUILDFLAGS and buildflags.mk inclusion as
        debhelper is already takes care of them.
  * Move lib to /lib/[arch triplet]
    * debian/rules, debian/libmnl{0,-dev}.install:
      - Move lib to /lib/[arch triplet]
      - Add LIBMNL_SO_VER variable to declare the soname version for
        the libmnl-dev symlink creation.
    * debian/libmnl-dev.links:
      - Create the /usr/lib/[arch triplet]/libmnl.so symlink to lib.
    * debin/control:
      - Add dh-exec to build-deps as it is required by arch triplet static
        lib installation.
      - Add Vcs-{Browser,Git} URL.
    * debian/NEWS:
      - Add the information for reason why move the lib.
    * debian/libmnl{0,-dev}.dirs:
      - Drop as unneeded.
  * debian/libmnl0.symbols: Add symbol control file.
  * debian/rules: Add stack protector (hardening) compiler flag
  * debian/copyright: Update copyright years
  * Switch debian package compression to xz
  * Get library so version on build-time instead of hardcoded
    * debian/rules:
      - Get library so version on build-time and pass it to dh_link for
        symlinks installation.
  * Add cryptographic upstream tarball signature verification
  * debian/control: Change Architecture to linux-any (Closes: #745225)
  * Add patch to fix FTBFS on ppc64el
    * debian/patches/update-libtool.patch:
      - Thanks to Erwan Prioul for pointing to Adam Conrad's patch.
        (Closes: #748760)
  * Drop directly mentions multiarch-support in a Pre-Depends
    * debian/control:
      - Drop directly mentions multiarch-support in a Pre-Depends.
        This is the preparation for the eglibc to glibc transition.
        See: #747439 - lintian: warn about Pre-Depends: multiarch-support
                       in debian/control

 -- Neutron Soutmun <neo.neutron@gmail.com>  Sun, 20 Apr 2014 20:25:34 +0700

libmnl (1.0.3-3) unstable; urgency=low

  * Fix typo in pre-depends
    Closes: #675323

 -- Anibal Monsalve Salazar <anibal@debian.org>  Thu, 31 May 2012 19:38:34 +1000

libmnl (1.0.3-2) unstable; urgency=low

  [ Neutron Soutmun ]
  * debian/rules:
    - Also build static library.
  * debian/libmnl{0,-dev}.install:
    - Update installation path to MultiArch libdir path.
  * Add watch file
  * Use autoreconf
    * debian/control:
      - Add dh-autoreconf to build-deps.
  * Drop duplicated "Section" filed in control file
    * debian/control:
      - Drop duplicated "Section" field of libmnl0 package as it could be
        inherited from source package. It prevents missing duplicate places
        that need to be fixed if the value ever changes.
  * Update long description with extended description
    * debian/control:
      - Update long description with extended description which explains
        what is in each package.
  * Update copyright format url to version 1.0

  [ Anibal Monsalve Salazar ]
  * New co-maintainer: Neutron Soutmun
  * Dm-Upload-Allowed: yes

 -- Anibal Monsalve Salazar <anibal@debian.org>  Thu, 31 May 2012 09:17:38 +1000

libmnl (1.0.3-1) unstable; urgency=low

  * New upstream version 1.0.3
    Closes: #656870
  * Fix out-of-date-standards-version
  * DH compatibility is 9
  * Add multiarch support
  * Use hardening options

 -- Anibal Monsalve Salazar <anibal@debian.org>  Mon, 28 May 2012 13:03:37 +1000

libmnl (1.0.1-1) unstable; urgency=low

  * New upstream release
  * Upload to unstable

 -- Anibal Monsalve Salazar <anibal@debian.org>  Sat, 01 Jan 2011 18:59:00 +1100

libmnl (0.0.0~20101124-1) experimental; urgency=low

  * Initial release
    Closes: 604757

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 24 Nov 2010 12:14:40 +1100
